﻿angular.module('travelstory.controllers')

.controller('ViewstoryCtrl', ['$scope', '$state', '$stateParams', '$ionicModal', '$ionicSlideBoxDelegate', '$ionicHistory', '$ionicPlatform', 'Story', 'DateService', 'GoogleService', 'ErrorHandler',
    function ($scope, $state, $stateParams, $ionicModal, $ionicSlideBoxDelegate, $ionicHistory, $ionicPlatform, Story, DateService, GoogleService, Err) {

        $scope.story = $stateParams.story;
        Story.getStoryDetails($scope.story.id).then(function (response) {
            if (Err.errorRequest(response.data)) {
                $scope.photos = response.data.content;
                $ionicSlideBoxDelegate.update();
            }
        });
        //Display location on map view
        $scope.displayLocation = function () {
            $ionicModal.fromTemplateUrl('templates/popup-templates/map-view.html', {
                scope: $scope,
                animation: 'slide-in-up',
                backdropClickToClose: false
            }).then(function (modal) {
                $scope.mapView = modal;
                $scope.mapView.show().then(function (result) {
                    GoogleService.reverseGeocode($scope.story.location).then(function (result) {
                        if (typeof result.coordinate !== 'undefined') {
                            var lat = result.coordinate.latitude;
                            var lng = result.coordinate.longitude;
                            var myLatlng = new google.maps.LatLng(lat, lng);
                            var map = new google.maps.Map(document.getElementById("map"), {
                                center: myLatlng,
                                zoom: 10,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            });
                            $scope.map = map;
                            var marker = new google.maps.Marker({
                                position: myLatlng,
                                map: map,
                                title: 'Location'
                            });
                        } else {
                            Err.notify(result.message);
                        }
                    }, function (result) {
                        Err.notify(result.message);
                    });
                    
                });
            });
        };

        $scope.closeMap = function () {
            $scope.mapView.hide();
            $scope.mapView.remove();
        };

        var previous = $ionicHistory.backTitle();   //Workaround to make the navigation logic
        $scope.goBack = function () {
            if (previous === "Edit Story") {
                $state.go('editstory', { story: $scope.story });
                $ionicHistory.clearHistory();
            } else {
                $ionicHistory.goBack();
            }
        };

        $ionicPlatform.onHardwareBackButton(function () {
            if (previous === "Edit Story") {
                $state.go('editstory', { story: $scope.story });
                $ionicHistory.clearHistory();
            } else {
                $ionicHistory.goBack();
            }
        });

        $scope.epochToDate = function (val) {
            var date = DateService.epochToDate(val).split(',');
            var time = {};
            time.date = date[0];
            time.time = date[1];
            return time;       
        };
    }]);