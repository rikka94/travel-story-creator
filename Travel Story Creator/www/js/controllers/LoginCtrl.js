angular.module('travelstory.controllers')

.controller('LoginCtrl', ['$scope', '$state', '$http', '$ionicHistory', 'UserService', 'ErrorHandler', '$cordovaOauth',
    function ($scope, $state, $http, $ionicHistory, UserService, Err, $cordovaOauth) {
        
        var $this = this;   //To expose controller private function for testing purpose
        $scope.login = {};
        $scope.register = {};
        $scope.operation = "Sign In";

        $scope.login.login = function (email, password) {
            if ($this.validateEmail(email)) {
                UserService.login(email, password).then(function (response) {
                    var data = response.data;
                    if (Err.errorRequest(data)) {
                        UserService.setAccess(data.access_token);
                        UserService.setSession();
                        $state.go("tabs.mystory");
                        $ionicHistory.clearHistory();
                        $ionicHistory.nextViewOptions({
                            disableAnimate: false,
                            disableBack: true
                        });
                    }
                });
            }
        };

        $scope.register.register = function (email, username, password, confirmPassword) {
            if (password === confirmPassword) {
                if ($this.validateEmail(email) && $this.validateName(username) && $this.validatePassword(password)) {
                    UserService.register(email, username, password)
                    .then(function (response) {
                        var data = response.data;
                        if (Err.errorRequest(data)) {
                            Err.notify(data.message);
                            $scope.login.login(email, password);
                        }
                    });
                }
            } else {
                Err.notify("Password not matched...");
            }
        };

        $scope.login.facebookLogin = function () {
            var email, name, id;
            $cordovaOauth.facebook("964729270246318", ["email"]).then(function (result) {
                localStorage.facebook = angular.toJson(result.access_token);
                $http.get("https://graph.facebook.com/v2.4/me", { params: { access_token: result.access_token, fields: "id,name,email", format: "json" } })
                    .then(function (result) {
                        email = result.data.email;
                        name = result.data.name;
                        id = result.data.id;
                        //Attempt to Login
                        UserService.login(email, id).then(function (response) {
                            if (response.data.error) {
                                UserService.register(email, name, id).then(function (response) {
                                    if (Err.errorRequest(response.data)) {
                                        $scope.login.login(email, id);
                                    }
                                });
                            } else {
                                UserService.setAccess(response.data.access_token);
                                UserService.setSession();
                                $state.go("tabs.mystory");
                                $ionicHistory.clearHistory();
                                $ionicHistory.nextViewOptions({
                                    disableAnimate: false,
                                    disableBack: true
                                });
                            }
                        });
                    }, $this.error);
            }, $this.error);
        };

        $scope.login.googleLogin = function () {
            var email, name, id;
            $cordovaOauth.google("423423309631-2pkrd41o319al9hc09loa7oman0ovthj.apps.googleusercontent.com", ["email"]).then(function (result) {
                localStorage.google = angular.toJson(result.access_token);
                $http.get("https://www.googleapis.com/plus/v1/people/me?access_token=" + result.access_token)
                .then(function (response) {
                    email = response.data.emails[0].value;
                    name = response.data.displayName;
                    id = response.data.id;
                    //Attempt to Login
                    UserService.login(email, id).then(function (response) {
                        if (response.data.error) {
                            UserService.register(email, name, id).then(function (response) {
                                if (Err.errorRequest(response.data)) {
                                    $scope.login.login(email, id);
                                }
                            });
                        } else {
                            UserService.setAccess(response.data.access_token);
                            UserService.setSession();
                            $state.go("tabs.mystory");
                            $ionicHistory.clearHistory();
                            $ionicHistory.nextViewOptions({
                                disableAnimate: false,
                                disableBack: true
                            });
                        }
                    });
                }, $this.error);
            }, $this.error);
        };

        this.validateEmail = function (email) {
            var EMAIL_REGEXP = /^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            if (!EMAIL_REGEXP.test(email)) {
                Err.notify("Invalid Email Format");
                return false;
            }
            return true;
        };

        this.validateName = function (name) {
            if (name.length > 20 || name.length < 6) {
                Err.notify("Username must be at least six characters and at most 20 characters..");
                return false;
            }
            return true;
        };

        this.validatePassword = function (password) {
            if (password.length < 6) {
                Err.notify("Password must be at least more than six characters!");
                return false;
            }
            return true;
        };
        
        //Facebook / Google login went wrong
        this.error = function () {
            Err.notify("Something went wrong");
        };
    }]);

