﻿angular.module('travelstory.controllers')

.controller('EditstoryCtrl', ['$scope', '$rootScope', '$state', '$stateParams', '$ionicScrollDelegate', '$ionicPlatform', '$ionicNavBarDelegate', '$ionicActionSheet', '$ionicPopup', '$cordovaCamera', '$cordovaImagePicker', 'Story', 'DateService', 'FileService', 'GoogleService', 'ErrorHandler',
    function ($scope, $rootScope, $state, $stateParams, $ionicScrollDelegate, $ionicPlatform, $ionicNavBarDelegate, $ionicActionSheet, $ionicPopup, $cordovaCamera, $cordovaImagePicker, Story, DateService, FileService, GoogleService, Err) {

        $scope.story = $stateParams.story;
        $scope.photos = {};
        $scope.input = {};
        $scope.isEditing = {};

        $rootScope.$on('onReload', function () {
            Story.getStoryDetails($scope.story.id)
            .then(function (response) {
                var photos = {};
                if (Err.errorRequest(response.data, false)) {
                    photos = DateService.sortByDate(response.data.content);
                }
                $scope.photos = photos;
                $ionicScrollDelegate.resize();
            });
        });
        $rootScope.$broadcast('onReload');

        //Two ways of adding new photos into the story
        $scope.importPicture = function () {
            $cordovaImagePicker.getPictures({
                maximumImagesCount: 1,
                width: 300,
                height: 300,
                quality: 70
            }).then(importSuccess, pluginError);
        };

        $scope.takePicture = function () {
            $cordovaCamera.getPicture({
                quality: 70,
                targetHeight: 300, targetWidth: 300,
                encodingType: Camera.EncodingType.JPEG,
                sourceType: Camera.PictureSourceType.CAMERA,
                destinationType: Camera.DestinationType.NATIVE_URI,
                correctOrientation: true
            }).then(cameraSuccess, pluginError);
        };

        var importSuccess = function (results) {
            //Default location as parent story location and time
            var location = $scope.story.location;
            var epochTime = $scope.story.startTime;
            FileService.getFileSize(results[0]).then(function (result) {
                if (result.size < 2097152) {
                    FileService.extractExif(results[0]).then(function (result) {    //EXIF data is available
                        if (result.status) {
                            var tags = result.value;
                            var val = tags.DateTimeOriginal.value;
                            var epochTime = DateService.dateToEpoch(val);

                            if (tags.GPSLatitude !== undefined) {                   //GPS location is available
                                var lat = tags.GPSLatitude.description;
                                var lng = tags.GPSLongitude.description;
                                GoogleService.geocode(lat, lng).then(function (result) {
                                    if (result.status) {
                                        location = result.location;
                                        Story.addPhoto(results[0], $scope.story.id, epochTime, location).then(processResult, error, progress);
                                    } else {                                            //Unable to geocode gps into address
                                        Story.addPhoto(results[0], $scope.story.id, epochTime, location).then(processResult, error, progress);
                                    }
                                });
                            } else {
                                //unsuccess geocode scenario two
                                Story.addPhoto(results[0], $scope.story.id, epochTime, location).then(processResult, error, progress);
                            }
                        } else {
                            Err.notify("Unable to access the file");
                        }
                    }, function (result) {                                                                     //No EXIF data found in the photo
                        Story.addPhoto(results[0], $scope.story.id, epochTime, location).then(processResult, error, progress);
                    });
                } else {
                    Err.notify("File size must be less than 2 megabytes...");
                }
            }, getFileError);
        };

        var cameraSuccess = function (data) {
            var location, epochTime;
            FileService.getFileSize(data).then(function (result) {
                if (result.size < 2097152) {
                    FileService.extractExif(data).then(function (result) {
                        if (result.status) {
                            var tags = result.value;
                            var val = tags.DateTime.value;
                            location = $scope.story.location; //Default location as parent story location
                            epochTime = DateService.dateToEpoch(val);

                            if (tags.GPSLatitude !== undefined) {
                                var lat = tags.GPSLatitude.description;
                                var lng = tags.GPSLongitude.description;

                                GoogleService.geocode(lat, lng).then(function (result) {
                                    if (result.status) { //Location found
                                        location = result.location;
                                        Story.addPhoto(data, $scope.story.id, epochTime, location).then(processResult, error, progress);
                                    } else { //No location found
                                        Story.addPhoto(data, $scope.story.id, epochTime, location).then(processResult, error, progress);
                                    }
                                });
                            } else {
                                //unsuccess geocode scenario two
                                Story.addPhoto(data, $scope.story.id, epochTime, location).then(processResult, error, progress);
                            }
                        } else {
                            Err.notify("Unable to access the file");
                        }
                    }, function (result) {                                                                     //No EXIF data found in the photo
                        Story.addPhoto(data, $scope.story.id, epochTime, location).then(processResult, error, progress);
                    });
                } else {
                    Err.notify("File size must be less than 2 megabytes...");
                }
            }, getFileError);
        };

        var pluginError = function (error) {
            Err.notify(error);
        };

        var getFileError = function (result) {
            Err.notify(result.message);
        };

        //handle success callback
        var processResult = function (result) {
            var response = angular.fromJson(result.response);
            if (Err.errorRequest(response)) {
                Err.notify(response.message);
                $rootScope.$broadcast('onReload');
            }
        };

        //handle error callback
        var error = function (err) {
            Err.notify('Error uploading photo to server !');
        };

        //handle the progress
        var progress = function (progress) {
            if ((progress.loaded / progress.total) * 100 !== 100) {
                Err.wait();
            } else {
                Err.stop();
            }
        };

        //For editing and canceling editing of description
        $scope.startEdit = function (index) { $scope.isEditing[index] = true; };
        $scope.cancelEdit = function (index) { $scope.isEditing[index] = false; };

        $scope.updatePhotoDescription = function (photo_id, description, index) {
            Story.updateDescription(photo_id, description)
            .then(function (response) {
                if (Err.errorRequest(response.data)) {
                    $rootScope.$broadcast('onReload');
                    $scope.isEditing[index] = false;
                }
            });
        };

        $scope.removePhoto = function (photo_id) {
            $ionicPopup.confirm({ title: 'Removing photo', template: 'Are you sure you want to remove this photo ?' }).then(function (res) {
                if (res) {
                    Story.deletePhoto(photo_id)
                    .then(function (response) {
                        if (Err.errorRequest(response.data)) {
                            $rootScope.$broadcast('onReload');
                        }
                    });
                }
            });
        };

        $scope.completeEdit = function () {
            $ionicActionSheet.show({
                buttons: [{ text: 'Post Your Story' }, { text: 'Preview Your Story' }],
                destructiveText: 'Delete Current Story', titleText: 'Options', cancelText: 'Cancel',
                buttonClicked: function (index) {
                    if (index === 0) { postStory(); return true; }
                    else if (index == 1) { previewStory(); return true; }
                },
                destructiveButtonClicked: function () { deleteStory(); return true; }
            });
        };

        function postStory() {
            if ($scope.photos.length > 0) {
                $ionicPopup.show({
                    template: '<div class="list"><input type="date" ng-model="input.date"/><input type="time" ng-model="input.time" /></div>',
                    title: 'End Time',
                    subTitle: 'When was this event ended ?',
                    scope: $scope,
                    buttons: [{ text: 'Cancel' },
                        {
                            text: '<b>Post Story</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                if (!$scope.input.date || !$scope.input.time) {
                                    e.preventDefault();
                                } else {
                                    return new Date($scope.input.date).getTime() + new Date($scope.input.time).getTime();
                                }
                            }
                        }
                    ]
                }).then(function (res) {
                    var end_time = res / 1000;
                    Story.postStory($scope.story.id, end_time).then(function (response) {
                        if (Err.errorRequest(response.data)) {
                            Err.notify("Story posted");
                            $state.go('tabs.mystory');
                        }
                    });
                });
            } else {
                Err.notify("Must have at least one photo added before a story can be posted..");
            }
        }

        function previewStory() { $state.go('viewstory', { story: $scope.story }); }

        function deleteStory() {
            $ionicPopup.confirm({ title: 'Delete Story', template: 'Are you sure you want to delete this story' }).then(function (res) {
                if (res) {
                    Story.deleteStory($scope.story.id)
                    .then(function (response) {
                        if (Err.errorRequest(response.data)) {
                            Err.notify("Story Deleted");
                            $state.go('tabs.mystory');
                        }
                    });
                }
            });
        }

        $scope.epochToDate = function (epoch) { return DateService.epochToDate(epoch); };
        //Alter the default behaviour of android built in back button
        $ionicPlatform.onHardwareBackButton(function () {
            $state.go('tabs.mystory');
        });
        $ionicNavBarDelegate.showBackButton(false);         //Hide the back button when navigate back from previewing
    }]);