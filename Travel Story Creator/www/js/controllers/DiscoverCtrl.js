﻿angular.module('travelstory.controllers')

.controller('DiscoverCtrl', ['$scope', '$state', '$ionicModal', '$ionicNavBarDelegate', '$cordovaSocialSharing', 'Story', 'ErrorHandler', 'DEFAULT_PP',
    function ($scope, $state, $ionicModal, $ionicNavBarDelegate, $cordovaSocialSharing, Story, Err, DEFAULT_PP) {

        $scope.defaultProfilePicture = DEFAULT_PP;
        $scope.isSearching = false;
        $scope.result = {};
        Story.getFriendStory()
        .then(function (response) {
            if (Err.errorRequest(response.data, false)) {
                $scope.friendStory = response.data.content;
            }
        });

        $scope.$on('$ionicView.enter', function () {
            if (modalActive) {
                $scope.modal.show();
            }
        });

        $scope.viewStory = function (story) {
            $state.go('viewstory', { story: story });
            $scope.modal.hide();
        };

        $scope.shareStory = function (story) {
            var link = "http://128.199.96.57/view_story/?story_id=" + story.id;
            $cordovaSocialSharing.shareViaFacebook("Share The Story", null, link)
                .then(function (result) {
                    Err.notify("Successfully shared to Facebook !");
                }, function (error) {
                    Err.notify("Something happened!");
                });
        };

        $scope.displaySearchBar = function () {
            $ionicNavBarDelegate.showBar(false);
            $scope.isSearching = true;
        };

        $scope.hideSearchBar = function () {
            $ionicNavBarDelegate.showBar(true);
            $scope.isSearching = false;
        };

        var modalActive = false;
        $scope.search = function () {
            if ($scope.searchTerm) {
                Story.getStoriesByLocation($scope.searchTerm).then(function (response) {
                    if (Err.errorRequest(response.data, true, "No results found..")) {
                        $scope.result.story = response.data.content;
                        $ionicModal.fromTemplateUrl('templates/popup-templates/search-result.html', {
                            scope: $scope,
                            animation: 'slide-in-up',
                            backdropClickToClose: false
                        }).then(function (modal) {
                            $scope.modal = modal;
                            $scope.modal.show();
                            modalActive = true;
                        });
                    }
                });
            } else {
                Err.notify('Please enter location to search...');
            }
        };

        $scope.hideSearchResult = function () {
            $scope.modal.hide();
            $scope.modal.remove();
            modalActive = false;
        };
    }]);