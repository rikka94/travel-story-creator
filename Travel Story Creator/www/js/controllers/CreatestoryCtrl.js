angular.module('travelstory.controllers')
    .controller('CreatestoryCtrl', ['$scope', '$state', '$cordovaCamera', '$cordovaImagePicker', '$ionicHistory', '$cordovaGeolocation', '$ionicPopup', 'Story', 'GoogleService', 'DateService', 'FileService', 'ErrorHandler',
    function ($scope, $state, $cordovaCamera, $cordovaImagePicker, $ionicHistory, $cordovaGeolocation, $ionicPopup, Story, GoogleService, DateService, FileService, Err) {

        $scope.input = {};
        var actions = {};
        ionic.Platform.ready(function () {

            (function initializeTimeAndLocation(input) {
                //Get the current date time
                var currentDatetime = DateService.getCurrentTime();
                input.date = (currentDatetime.split(" "))[0];
                input.time = (currentDatetime.split(" "))[1];

                $cordovaGeolocation
                  .getCurrentPosition({ maximumAge: 60000, timeout: 31000, enableHighAccuracy: true })
                  .then(function (position) {
                      var lat = position.coords.latitude,
                      long = position.coords.longitude;
                      var result = GoogleService.geocode(lat, long);

                      result.then(function (result) {
                          if (result.status) {
                              input.storyLocation = result.location;
                          }
                      });
                  });
            })($scope.input);

            $scope.selectAction = function () {
                actions = $ionicPopup.show({
                    template: "<div class='list'><a class='item' ng-click='takePicture()'>Camera</a>" +
                        "<a class='item' ng-click='importPicture()'>Pick From Album</a></div>",
                    title: 'Choose an Action to perform',
                    scope: $scope,
                    buttons: [{ text: 'Cancel' }]
                });
            };

            $scope.takePicture = function () {
                $cordovaCamera.getPicture({
                    quality: 70,
                    encodingType: Camera.EncodingType.JPEG,
                    targetHeight: 300,
                    targetWidth: 300,
                    correctOrientation: true
                }).then(function (data) {
                    actions.close();
                    $scope.input.pictureUrl = data;
                }, error);
            };

            $scope.importPicture = function () {
                $cordovaImagePicker.getPictures({
                    maximumImagesCount: 1,
                    width: 300,
                    height: 300,
                    quality: 70,
                }).then(function (results) {
                    actions.close();
                    $scope.input.pictureUrl = results[0];
                }, error);
            };

            var error = function (error) {
                actions.close();
                Err.unknownError();
            };

            $scope.createStory = function (input) {

                var isFormCompleted = !!(input.storyTitle) && !!(input.storyLocation) && !!(input.date) && !!(input.time) && !!(input.pictureUrl);
                //Instantiate a Date object by combining date and time and convert them to UNIX timestamp
                var startTime = (new Date(input.date + ' ' + input.time)).getTime() / 1000;

                FileService.getFileSize(input.pictureUrl).then(function (result) {
                    if (result.size < 2097152) {
                        if (isFormCompleted) {
                            Story.createStory(input.pictureUrl, input.storyTitle, input.storyLocation, startTime)
                            .then(function (result) {
                                $state.go('tabs.mystory');
                                $ionicHistory.clearHistory();
                                $ionicHistory.nextViewOptions({
                                    disableAnimate: false,
                                    disableBack: true
                                });
                            }, function (err) {
                                Err.notify('Something happened');
                            }, function (progress) {
                                if ((progress.loaded / progress.total) * 100 !== 100) {
                                    Err.wait();
                                } else {
                                    Err.stop();
                                }
                            });
                        } else {
                            Err.notify("Please fill in all the field required field !");
                        }
                    } else {
                        Err.notify("File size must be less than 2 megabytes...");
                    }
                });
            };
        });

        $scope.editTitle = function () {
            var editTitle = $ionicPopup.prompt({
                template: 'Enter Title of Story',
                title: 'Story Title',
                inputPlaceholder: 'Enter title here',
            });

            editTitle.then(function (res) {
                $scope.input.storyTitle = res;
            });
        };

        $scope.editLocation = function () {
            var editTitle = $ionicPopup.prompt({
                template: 'Enter Location of Story',
                title: 'Story Location',
                inputPlaceholder: 'Enter location here'
            });

            editTitle.then(function (res) {
                $scope.input.storyLocation = res;
            });
        };

        $scope.setDate = function (val) {
            if (typeof (val) !== 'undefined') {
                $scope.input.date = DateService.formatDate(val);
            }
        };

        $scope.setTime = function (val) {
            if (typeof (val) !== 'undefined') {
                var hours = parseInt(val / 3600) % 24;
                var minutes = parseInt(val / 60) % 60;
                var seconds = val % 60;

                $scope.input.time = (hours < 10 ? "0" + hours : hours) + ":" +
                    (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
            }
        };
    }]);