﻿angular.module('travelstory.controllers')

//Injecting dependencies
.controller('MystoryCtrl', ['$scope', '$state', 'Story', 'ErrorHandler', '$ionicSlideBoxDelegate', '$rootScope', '$ionicModal', '$ionicPopup', '$cordovaSocialSharing',
    function ($scope, $state, Story, Err, $ionicSlideBoxDelegate, $rootScope, $ionicModal, $ionicPopup, $cordovaSocialSharing) {

        var storyType = 'complete';                 //True to indicate the story is completed story, false indicate editing story
        var completedStory = [];
        var editingStory = [];

        $rootScope.$on('storyListUpdated', function () {
            completedStory = Story.getCompletedStories();
            editingStory = Story.getEditingStories();
            $scope.getList = function () {
                if (storyType === 'complete')
                    return completedStory;
                else if (storyType === 'edit')
                    return editingStory;
            };
            $ionicSlideBoxDelegate.update();
        });

        $scope.$on("$ionicView.enter", function () {
            Story.setStoryList();
        });

        $scope.displayComplete = function () {
            storyType = 'complete';
            Story.setStoryList();
            $ionicSlideBoxDelegate.slide(0);
        };

        $scope.displayEdit = function () {
            storyType = 'edit';
            Story.setStoryList();
            $ionicSlideBoxDelegate.slide(0);
        };

        $scope.editStory = function (story) {
            $state.go('editstory', { story: story });
        };

        $scope.createStory = function () {
            $state.go('createstory');
        };

        $scope.viewStory = function (story) {
            $state.go('viewstory', { story: story });
        };

        $scope.deleteStory = function (story_id, index) {
            var confirmDelete = $ionicPopup.confirm({
                title: 'Story Deletion',
                template: 'Confirm to delete story?'
            });
            confirmDelete.then(function (res) {
                if (res) {
                    $ionicSlideBoxDelegate.slide(index - 1);
                    Story.deleteStory(story_id)
                    .then(function (response) {
                        if (Err.errorRequest(response.data)) {
                            Err.notify(response.data.message);
                            Story.setStoryList();
                        }
                    });
                }
            });
        };

        $ionicModal.fromTemplateUrl('templates/popup-templates/modal-story-sharing.html', {
            scope: $scope,
            animation: 'slide-in-up',
            backdropClickToClose: false
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.shareStory = function (story) {
            var link = "http://128.199.96.57/view_story/?story_id=" + story.id;
            $cordovaSocialSharing.shareViaFacebook("Share this story", null, link)
                .then(function (result) {
                    Err.notify("Successfully shared to Facebook !");
                }, function (error) {
                    Err.notify("Something happened!");
                });
        };
    }]);