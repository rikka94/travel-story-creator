﻿angular.module('travelstory.services')

.factory('UserService', ['$http', '$rootScope', 'SERVER_URL', '$cordovaFileTransfer',function ($http, $rootScope, SERVER_URL, $cordovaFileTransfer) {

    var services = {

        login: function (email, password) {
            var loginCredential = { email: email, password: password };
            return $http.post(SERVER_URL + 'login', loginCredential);
        },

        register: function (email, name, password) {
            var registrationInfo = { email: email, name: name, password: password };
            return $http.post(SERVER_URL + 'register', registrationInfo);
        },

        get: function (user_id) {
            return $http.get(SERVER_URL + 'getUser/' + user_id);
        },

        getUserByEmail: function(email){
            return $http.get(SERVER_URL + 'getUserByEmail/' + email);
        },

        updateStatus: function (status) {
            var data = { about: status };
            return $http.put(SERVER_URL + 'updateProfile/' + this.getAccess(), data);
            
        },

        updateName: function (name) {
            var data = { name: name };
            return $http.put(SERVER_URL + 'updateProfile/' + this.getAccess(), data);
        },

        changePassword: function (password, newPassword) {
            var data = { password: password, newPassword: newPassword };
            return $http.put(SERVER_URL + 'updateProfile/' + this.getAccess(), data);
        },

        changeProfilePicture: function(profile_picture){
            return $cordovaFileTransfer.upload(SERVER_URL + "updateProfilePicture/" + this.getAccess(), profile_picture, {
                fileKey: 'profile_picture',
                chunkedMode: false,
            });
        },

        removeProfilePicture: function(){
            return $http.delete(SERVER_URL + 'removeProfilePicture/' + this.getAccess());
        },

        logout: function () {
            sessionStorage.user = null;
            localStorage.accessToken = null;
            if (localStorage.google !== null) localStorage.google = null;
            if (localStorage.facebook !== null) localStorage.facebook = null;
        },

        //set personal information upon login
        setSession: function () {
            this.get(this.getAccess())
            .then(function (response) {
                var user = response.data.content;
                sessionStorage.user = angular.toJson(user[0]);
                $rootScope.$broadcast('onUserUpdated');
            });
        },
        //get personal information
        getSession: function () {
            return angular.fromJson(sessionStorage.user);
        },

        setAccess: function (access_token) {
            localStorage.accessToken = angular.toJson(access_token);
        },

        getAccess: function () {
            return angular.fromJson(localStorage.accessToken);
        }
    };

    return services;
}]);