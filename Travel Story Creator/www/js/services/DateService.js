﻿angular.module('travelstory.services')

.factory('DateService', [function () {

    function compareDate(a, b) {
        x = a.time * 1000;
        y = b.time * 1000;

        return x - y;
    }

    return {
        epochToDate: function (epoch) {             //Convert epoch format date into datetime
            var date = new Date(epoch * 1000);
            var day = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            return day[date.getDay()] + ' ' + date.toLocaleString();
        },

        sortByDate: function (list) {
            if (list !== undefined) {
                list.sort(compareDate);
                return list;
            }
            return {};
        },

        getCurrentTime: function () {               //get current time in a proper format
            var now = new Date();
            var year = now.getFullYear();
            var month = now.getMonth() + 1;
            var day = now.getDate();
            var hour = now.getHours();
            var minute = now.getMinutes();
            var second = now.getSeconds();

            if (month.toString().length == 1) {
                month = '0' + month;
            }
            if (day.toString().length == 1) {
                day = '0' + day;
            }
            if (hour.toString().length == 1) {
                hour = '0' + hour;
            }
            if (minute.toString().length == 1) {
                minute = '0' + minute;
            }
            if (second.toString().length == 1) {
                second = '0' + second;
            }
            var dateTime = year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
            return dateTime;
        },

        formatDate: function (date) {       //Format any date format acceptable by Date class constructor into yy/MM/dd format 
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length == 1) month = '0' + month;
            if (day.length == 1) day = '0' + day;

            return [year, month, day].join('/');
        },

        dateToEpoch: function (val) {
            var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var ary = val.toString().split(' ');
            var date = ary[0].split(':');
            var time = ary[1].split(':');
            var dateTime = month[date[1] - 1] + ' ' + date[2] + ', ' + date[0] + ' ' + time[0] + ':' + time[1] + ':' + time[2];
            return (new Date(dateTime).getTime() /1000);
        }
    };
}]);