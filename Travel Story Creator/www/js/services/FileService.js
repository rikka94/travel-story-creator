﻿angular.module('travelstory.services')

.factory('FileService', ['$q', function ($q) {
    var services = {
        extractExif: function (url) {
            var deferred = $q.defer();
            var result = {};
            window.resolveLocalFileSystemURI(url, function (entry) {
                entry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (evt) {
                        var exif = new ExifReader();
                        exif.load(evt.target.result);
                        var allTags = exif.getAllTags();
                        result.status = true;
                        result.value = allTags;
                        deferred.resolve(result);
                    };
                    reader.readAsArrayBuffer(file.slice(0, 128 * 1024));
                }, function (error) {
                    result.status = false;
                    result.message = "something went wrong";
                    deferred.reject(result);
                });
            });
            setTimeout(function () {
                result.status = false;
                result.message = "Unable to extract date";
                deferred.reject(result);
            }, 2000);
            return deferred.promise;
        },

        getFileSize: function (url) {
            var deferred = $q.defer();
            var result = {};
            window.resolveLocalFileSystemURI(url, function (entry) {
                entry.file(function (file) {
                    result.status = true;
                    result.message = "Size retrieved";
                    result.size = file.size;
                    deferred.resolve(result);
                }, function (error) {
                    result.status = false;
                    result.message = "something went wrong";
                    deferred.reject(result);
                });
            });
            return deferred.promise;
        }
    };

    return services;
}]);