﻿angular.module('travelstory.services')

.factory('GoogleService', ['$q', function ($q) {

    var geocoder = new google.maps.Geocoder();
    
    return {
        //from latidude longitude to address
        geocode: function (lat, long) {
            var latlng = new google.maps.LatLng(lat, long);
            var deferred = $q.defer();
            var result = {};
            
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        result.location = results[0].formatted_address;
                        result.status = true;
                        result.message = "Geocoding success";
                        deferred.resolve(result);

                    } else {
                        result.status = false;
                        result.message = "Geocoding failed due to " + status;
                        deferred.reject(result);
                    }
                } else {
                    result.status = false;
                    result.message = "Something went wrong";
                    deferred.reject(result);
                }
             });

            return deferred.promise;
        },

        //From address back to latidude and longtidude
        reverseGeocode : function(address){
            var result = {};
            var deferred = $q.defer();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        result.coordinate = {};
                        result.coordinate.latitude = results[0].geometry.location.G;
                        result.coordinate.longitude = results[0].geometry.location.K;
                        result.status = true;
                        result.message = "Geocoding success";
                        deferred.resolve(result);

                    } else {
                        result.status = false;
                        result.message = "Geocoding failed due to " + status;
                        deferred.reject(result);
                    }
                } else {
                    result.status = false;
                    result.message = "Something went wrong";
                    deferred.reject(result);
                }
            });
            return deferred.promise;
        }
    };
}]);