describe('DateService', function(){
    beforeEach(angular.mock.module('travelstory'));
    
    var dateService;
    beforeEach(angular.mock.inject(function(DateService){
        dateService = DateService;
    }));
    
    it('should convert epoch time to proper date format', function(){
        var epoch = 0;
        //toLocaleString return value is browser dependant hence is untestable , however it should contains thursday
        expect(dateService.epochToDate(epoch)).toMatch(/Thursday/);
        epoch = 1440254233;
        expect(dateService.epochToDate(epoch)).toMatch(/Saturday/);
    });
    
    it('should revert date format to proper epoch time', function(){
        //These method are built on top of javascript Date class, behaviour might be different across different browsers/clients
        var date = "2015:07:31 19:53:35";
        expect(dateService.dateToEpoch(date)).toEqual(1438343615);
        date = "1970:1:1 08:00:00";
        expect(dateService.dateToEpoch(date)).toEqual(0);
    });
    
    it('should format date into yy/MM/dd', function(){
        var date = "Thu, 01 Jan 1970 00:00:00 GMT";
        expect(dateService.formatDate(date)).toEqual("1970/01/01");
    });
});