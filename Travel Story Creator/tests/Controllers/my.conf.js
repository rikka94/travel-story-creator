// Karma configuration
// Generated on Sat Aug 15 2015 20:33:09 GMT+0800 (Malay Peninsula Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        '../www/lib/angular/angular.min.js',
        '../www/lib/angular/angular-sanitize.min.js',
        '../www/lib/angular/angular-animate.min.js',
        '../www/lib/angular/angular-resource.min.js',
        '../www/lib/angular-ui/angular-ui-router.min.js',
        '../www/lib/ionic.min.js',
        '../www/lib/ionic-angular.min.js',
        '../www/lib/angular-mocks/angular-mocks.js',
        '../www/lib/ng-cordova.min.js',
        '../www/lib/ng-cordova-mocks.min.js',
        '../www/lib/external-library/*',
        '../www/js/app.js',
        '../www/js/services/*',
        '../www/js/directives/*',
        '../www/js/controllers/*',
        '**/*tests.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true
  })
}
