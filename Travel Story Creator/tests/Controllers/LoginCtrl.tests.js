describe('LoginCtrl', function() {
    beforeEach(angular.mock.module('travelstory'));

    var scope, state, http, ionicHistory, userService, errorHandler, cordovaOauth, ctrl;    
    

    beforeEach(angular.mock.inject(function($rootScope, $controller, SERVER_URL, $state, $http, $ionicHistory, UserService, ErrorHandler, $cordovaOauth){
        scope = $rootScope.$new();
        state = $state;
        http = $http;
        ionicHistory = $ionicHistory;
        userService = UserService;
        errorHandler = ErrorHandler;
        cordovaOauth = $cordovaOauth;
        
        ctrl = $controller('LoginCtrl', {
            $scope: scope,
            $state: state,
            $http: http,
            $ionicHistory: ionicHistory,
            UserService: userService,
            ErrorHandler: errorHandler,
            $cordovaOauth: cordovaOauth
        });
    }));
    
    it('should have operation to keep track of current screen', function() {
        expect(scope.operation).toEqual('Sign In');
    });
    
    it('should check for invalid email format', function(){
        expect(ctrl.validateEmail('zzzz')).toEqual(false);
        expect(ctrl.validateEmail('example@gmail')).toEqual(false);
        expect(ctrl.validateEmail('aaaaa.aaaa')).toEqual(false);
        //Only 2 to 4 characters allowed after '@' , number of '.' however is not limited
        expect(ctrl.validateEmail('aaaaa@aaaaaa.aaaaa')).toEqual(false);
        expect(ctrl.validateEmail('aaaa@aaaa.a')).toEqual(false);
    });
    
    it('should check for valid email format', function(){
        expect(ctrl.validateEmail('aaaaa@dsds.csd')).toEqual(true);
        expect(ctrl.validateEmail('example@gmail.com')).toEqual(true);
        expect(ctrl.validateEmail('example@gmail.edu.my')).toEqual(true);
    });
    
    it('should check for valid password length', function(){
        //Boundary values test, no limit to password length, max 255 database column max length
        expect(ctrl.validatePassword('aaaaa')).toEqual(false);
        expect(ctrl.validatePassword('aaaaaa')).toEqual(true);
        expect(ctrl.validatePassword('12345678900987654321')).toEqual(true);
    });
    
    it('should check to valid username length', function(){
        //Boundary values test, max 20 characters, min 6 characters
        expect(ctrl.validateName('aaaaa')).toEqual(false);
        expect(ctrl.validateName('aaaaaa')).toEqual(true);
        expect(ctrl.validateName('123456789009876543211')).toEqual(false);
        expect(ctrl.validateName('1234567890098765432')).toEqual(true);
    });
    
});