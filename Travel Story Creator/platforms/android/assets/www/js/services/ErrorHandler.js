﻿angular.module('travelstory.services')

.factory('ErrorHandler', ['$ionicLoading', function ($ionicLoading) {
    
    var indeterminateLoading = {};
    //Method overloading to allow custom message
    var errorRequest = [
            //default , display message from server when error
            function (data) {
                if (data.error) {
                    $ionicLoading.show({ template: data.message, noBackdrop: true, duration: 1000 });
                    return false;
                }
                return true;
            },
            //displayMessage = false, don't display anything , simply return false
            function (data, displayMessage) {
                if (data.error) {
                    if (displayMessage) { $ionicLoading.show({ template: data.message, noBackdrop: true, duration: 1000 }); }
                    return false;
                }
                return true;
            },
            //displayMessage = true, display a custom message
            function (data, displayMessage, message) {
                if (data.error) {
                    if (displayMessage) { $ionicLoading.show({ template: message, noBackdrop: true, duration: 1000 }); }
                    return false;
                }
                return true;
            }
    ];

    return {
        errorRequest: function (data, displayMessage, message) {
            return errorRequest[arguments.length - 1](data, displayMessage, message);
        },

        unknownError: function () {
            $ionicLoading.show({ template: 'Something went wrong !', noBackdrop: true, duration: 1000 });
        },

        notify: function (message) {
            $ionicLoading.show({ template: message, noBackdrop: true, duration: 1000 });
        },

        wait: function () {
            indeterminateLoading = $ionicLoading.show({
                template: "<ion-spinner icon='lines' class='spinner-positive'></ion-spinner>"
            });
        },

        stop: function () {
            indeterminateLoading.hide();
        }
    };
}]);