﻿angular.module('travelstory.services')
//Abstract services , not yet insert any datas of friendship in database
.factory('Friends', ['$http', '$rootScope', 'UserService', 'SERVER_URL', 'ErrorHandler',
    function ($http, $rootScope, UserService, SERVER_URL, Err) {

        var friendList = [];
        var suggestedList = [];
        var services = {
            setFriendList: function () {
                $http.get(SERVER_URL + 'getFriends/' + UserService.getAccess())
                .then(function (response) {
                    if (Err.errorRequest(response.data, false)) {
                        friendList = response.data.content;
                    } else {
                        friendList = [];
                    }
                    $rootScope.$broadcast('friendListUpdated');
                });
            },

            setSuggestedFriends: function () {
                $http.get(SERVER_URL + 'getSuggestedFriends/' + UserService.getAccess()).
                then(function (response) {
                    if (Err.errorRequest(response.data, false)) {
                        suggestedList = response.data.content;
                    } else {
                        suggestedList = [];
                    }
                    $rootScope.$broadcast('suggestedListUpdated');
                });
            },
            //Will return all friend which status = friend
            getFriends: function () {
                var friends = [];
                for (var i = 0; i < friendList.length; i++) {
                    if (friendList[i].status == "friend") {
                        friends.push(friendList[i]);
                    }
                }
                return friends;
            },

            //Return all friend which status = request
            getFriendRequests: function () {
                var friends = [];
                for (var i = 0; i < friendList.length; i++) {
                    if (friendList[i].status == "request") {
                        friends.push(friendList[i]);
                    }
                }
                return friends;
            },

            //Return all friend which status = sent
            getAddedFriends: function () {
                var friends = [];
                for (var i = 0; i < friendList.length; i++) {
                    if (friendList[i].status == "sent") {
                        friends.push(friendList[i]);
                    }
                }
                return friends;
            },

            //Return specific friend according to ID
            getFriend: function (user_id) {
                for (var i = 0 ; i < friendList.length; i++) {
                    if (friendList[i].id == user_id) {
                        return friendList[i];
                    }
                }
                return null;
            },

            addFriend: function (friend_id) {
                var data = {
                    user_id: UserService.getAccess(),
                    friend_id: friend_id
                };
                return $http.post(SERVER_URL + 'addFriend', data);
            },

            acceptFriend: function (friend_id) {
                var data = {
                    user_id: UserService.getAccess(),
                    friend_id: friend_id
                };
                return $http.put(SERVER_URL + 'updateFriend', data);
            },

            removeFriend: function (friend_id) {
                var user_id = UserService.getAccess();
                return $http.delete(SERVER_URL + 'deleteFriend/' + user_id + '/' + friend_id);
            },

            getSuggestedFriends: function () {
                return suggestedList;
            }
        };
        services.setFriendList();
        services.setSuggestedFriends();
        return services;
    }]);