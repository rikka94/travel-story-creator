﻿angular.module('travelstory.services')

.factory('Story', ['$http', 'SERVER_URL', 'UserService', 'ErrorHandler', '$rootScope', '$cordovaFileTransfer',
    function ($http,  SERVER_URL, UserService, Err, $rootScope, $cordovaFileTransfer) {
        var storyList = [];
        var services = {

            getStoriesById: function (user_id) {                        //Get all story belong to particular user
                return $http.get(SERVER_URL + 'getStories/' + user_id);
            },

            getStoryById: function (story_id) {                         //Get story information of a particular story 
                return $http.get(SERVER_URL + 'getStory/' + story_id);
            },

            setStoryList: function () {                                             //Set stories into a story list
                $http.get(SERVER_URL + 'getStories/' + UserService.getAccess())
                .then(function (response) {
                    var data = response.data;
                    if (Err.errorRequest(data)) {
                        storyList = data.content;
                    } else {
                        storyList = [];
                    }
                    $rootScope.$broadcast('storyListUpdated');
                });
            },

            getStories: function () {               ////Get stories belong to current logged in user
                return storyList;
            },

            getCompletedStories : function (){
                var list = [];
                for (var i = 0 ; i < storyList.length; i++) {
                    if (storyList[i].endTime !== null) {
                        list.push(storyList[i]);
                    } 
                }
                return list;
            },

            getEditingStories : function (){
                var list = [];
                for (var i = 0 ; i < storyList.length; i++) {
                    
                    if (storyList[i].endTime === null) {
                        list.push(storyList[i]);
                    }
                }
                return list;
            },

            getStoriesByLocation: function ($locationToSearch) {         //Searching all story at particular location matched the search term
                return $http.get(SERVER_URL + 'getStoriesByLocation/' + UserService.getAccess() + '/' + $locationToSearch);
            },

            getFriendStory: function () {                               //Get all stories of friends 
                return $http.get(SERVER_URL + 'getFriendStory/' + UserService.getAccess());
            },

            getStoryDetails: function (story_id) {                      //Get the details of story which is photos belong to particular story
                return $http.get(SERVER_URL + 'getPhotos/' + story_id);
            },

            createStory: function (imageUrl, title, location, startTime) {
                var params = {
                    user_id: UserService.getAccess(),
                    title: title,
                    location: location,
                    startTime: startTime
                };

                return $cordovaFileTransfer.upload(SERVER_URL + 'createStory', imageUrl, {
                    fileKey: 'cover_photo',
                    params: params,
                    chunkedMode: false
                });
            },

            updateStoryTitle: function (story_id, title) {
                var data = { title: title };
                return $http.put(SERVER_URL + 'updateStory/' + story_id, data);
            },

            updateStoryLocation: function (story_id, location) {
                var data = { location: location };
                return $http.put(SERVER_URL + 'updateStory/' + story_id, data);
            },

            updateStoryCoverPhoto: function (story_id, imageUrl) {
                return $cordovaFileTransfer.upload(SERVER_URL + 'updateCoverPhoto/' + story_id, imageUrl, {
                    fileKey: 'cover_photo',
                    chunkedMode: false
                });
            },

            postStory: function (story_id, end_time) {
                var data = { end_time: end_time };
                return $http.put(SERVER_URL + 'postStory/' + story_id, data);
            },

            deleteStory: function (story_id) {
                return $http.delete(SERVER_URL + 'deleteStory/' + story_id);
            },

            addPhoto: function (imageUrl, story_id, time, location) {
                var params = {
                    story_id: story_id,
                    time: time,
                    location: location
                };

                return $cordovaFileTransfer.upload(SERVER_URL + "addPhoto", imageUrl, {
                    fileKey: 'photo',
                    params: params,
                    chunkedMode: false,
                });
            },

            updateDescription: function (photo_id, description) {
                var data = { description: description };
                return $http.put(SERVER_URL + 'updatePhoto/' + photo_id, data);
            },

            deletePhoto: function (photo_id) {
                return $http.delete(SERVER_URL + 'deletePhoto/' + photo_id);
            }
        };
        services.setStoryList();
        return services;
    }]);