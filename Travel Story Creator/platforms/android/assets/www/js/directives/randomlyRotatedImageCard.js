﻿angular.module('travelstory.directives')

.directive('randomlyRotatedImageCard', [function () {
    return {
        restrict: 'E',
        scope: {
            image: '@ngImage',
            location: '@ngLocation',
            time: '@ngTime',
            date: '@ngDate',
            description: '@ngDescription'
        },
        template: '<div class="list card"><div class="item"><h2>{{location}}</h2><p>{{date}}</p><p>{{time}}</p></div><div class="item item-image item-body padding" style="{{style}}">\
                    <img ng-src="{{image}}" style="{{size}}"/><p>{{description}}</p></div></div>',

        link: function (scope, el, attr) {
            scope.style = 'transform: rotate(' + (Math.random() * 10 - 5) + 'deg)scale(0.9)';
            scope.size = 'height: 350px; width: 300px';
        }
    };
}]);