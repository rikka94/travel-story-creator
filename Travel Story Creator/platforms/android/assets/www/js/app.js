var app = angular.module('travelstory', ['ionic', 'travelstory.controllers', 'travelstory.services', 'travelstory.directives', 'ngCordova', 'ionic-datepicker', 'ionic-timepicker'], function ($httpProvider) {

    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

app.run(function ($ionicPlatform, $rootScope, $ionicLoading, ErrorHandler, UserService, $location) {
    $ionicPlatform.ready(function () {

        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleLightContent();
        }
    });

    if (UserService.getAccess() !== null) {
        UserService.setSession();
        $location.path('/tabs/mystory');
    }

    $rootScope.$on('onRequestSent', function () {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"/>',
            duration: 5000
        });
    });

    $rootScope.$on('requestError', function () {
        $ionicLoading.hide();
        ErrorHandler.notify('Connection problem occured...');
    });

    $rootScope.$on('onResponseGet', function () {
        $ionicLoading.hide();
    });

    $rootScope.$on('responseError', function () {
        $ionicLoading.hide();
        ErrorHandler.notify('No response from server...');
    });
});

app.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

    $ionicConfigProvider.tabs.position('bottom');
    $stateProvider

    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    })

    .state('createstory', {
        url: '/createstory',
        templateUrl: 'templates/createstory.html',
        controller: 'CreatestoryCtrl'
    })

    .state('editstory', {
        url: '/editstory',
        templateUrl: 'templates/editstory.html',
        controller: 'EditstoryCtrl',
        params: { story: null },
        cache: false
    })

    .state('viewstory', {
        url: '/viewstory',
        templateUrl: 'templates/viewstory.html',
        controller: 'ViewstoryCtrl',
        params: { story: null },
        cache: false
    })

    .state('tabs', {
        url: "/tabs",
        abstract: true,
        templateUrl: "templates/tabs.html",
    })

    .state('tabs.profile', {
        url: '/profile',
        views: {
            'tabs-profile': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl',
            }
        }
    })

    .state('tabs.friendprofile', {
        url: '/profile/friendprofile',
        views: {
            'tabs-profile': {
                templateUrl: 'templates/friendprofile.html',
                controller: 'FriendProfileCtrl'
            }
        },
        params: { friend: null }
    })

    .state('tabs.discover', {
        url: '/discover',
        views: {
            'tabs-discover': {
                templateUrl: 'templates/discover.html',
                controller: 'DiscoverCtrl',
            }
        }
    })

    .state('tabs.mystory', {
        url: '/mystory',
        views: {
            'tabs-mystory': {
                templateUrl: 'templates/mystory.html',
                controller: 'MystoryCtrl',
            }
        }
    });

    $urlRouterProvider.otherwise('/login');

    //Define interceptor to intercept all http request
    var interceptor = function ($q, $rootScope) {
        return {
            request: function (config) {
                $rootScope.$broadcast('onRequestSent');
                return config;
            },

            requestError: function (rejection) {
                $rootScope.$broadcast('requestError');
                return $q.reject(rejection);
            },

            response: function (response) {
                $rootScope.$broadcast('onResponseGet');
                return response;
            },

            responseError: function (rejection) {
                $rootScope.$broadcast('requestError');
                return $q.reject(rejection);
            }
        };
    };

    //Push the defined interceptor into http interceptor
    $httpProvider.interceptors.push(interceptor);
});


//Creating services, controller and directives dependancy
angular.module('travelstory.services', []);
angular.module('travelstory.controllers', []);
angular.module('travelstory.directives', []);
