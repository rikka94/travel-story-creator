﻿angular.module('travelstory.controllers')

.controller('ProfileCtrl', ['$scope', '$state', '$window', 'UserService', 'Friends', 'Story', 'ErrorHandler', 'DEFAULT_PP', '$rootScope', '$ionicActionSheet', '$ionicModal', '$ionicPopup', '$ionicHistory', '$cordovaCamera', '$cordovaImagePicker', '$cordovaSocialSharing',
    function ($scope, $state, $window, UserService, Friends, Story, Err, DEFAULT_PP, $rootScope, $ionicActionSheet, $ionicModal, $ionicPopup, $ionicHistory, $cordovaCamera, $cordovaImagePicker, $cordovaSocialSharing) {

        $scope.data = {};
        $scope.defaultProfilePicture = DEFAULT_PP;

        $rootScope.$on('friendListUpdated', function () {
            $scope.friendList = Friends.getFriends();
            $scope.requestList = Friends.getFriendRequests();
            $scope.addedList = Friends.getAddedFriends();
        });
        Friends.setFriendList();

        $rootScope.$on('storyListUpdated', function () {
            $scope.storyList = Story.getStories();
        });
        Story.setStoryList();

        $rootScope.$on('suggestedListUpdated', function () {
            $scope.suggestedList = Friends.getSuggestedFriends();
        });
        Friends.setSuggestedFriends();

        $rootScope.$on('onUserUpdated', function () {
            $scope.userInfo = UserService.getSession();
        });
        UserService.setSession();

        $scope.inviteFriend = function () {
            var searchedEmail;
            var searchByEmail = $ionicPopup.prompt({
                template: 'Email Invitation',
                title: "Enter your friend's email",
                inputPlaceholder: 'Email Address',
            });

            searchByEmail.then(function (res) {
                searchedEmail = res;
                if (res) {
                    UserService.getUserByEmail(res).then(function (response) {
                        if (!response.data.error) {
                            if (response.data.content[0].id != UserService.getAccess()) {   //If user type in his own address then not navigating
                                $scope.viewDetail(response.data.content[0]);
                            } else {
                                Err.notify("Email address belong to you...");
                            }
                        } else {
                            $ionicPopup.confirm({
                                title: 'No Account found',
                                template: 'Do you want to invite him through email ?'
                            }).then(function (res) {
                                if (res) {
                                    var message = "Come and Join me in this awesome application Travel Story Creator!";
                                    var subject = "Travel Story Creator Invitation";
                                    var toArr = [searchedEmail];
                                    $cordovaSocialSharing.shareViaEmail(message, subject, toArr, null, null, null).then(function (result) {
                                        Err.notify("Invitation sent");
                                    }, function (err) {
                                        Err.notify(err);
                                    });
                                }
                            });
                        }
                    });
                } else {
                    Err.notify("Field is empty !");
                }
            });
        };

        $scope.refreshList = function () {
            Friends.setSuggestedFriends();
        };

        $scope.friendOperation = function (friend_id, operation) {
            var result;
            switch (operation) {
                case 'add':
                    result = Friends.addFriend(friend_id);
                    break;
                case 'update':
                    result = Friends.acceptFriend(friend_id);
                    break;
                case 'delete':
                    result = Friends.removeFriend(friend_id);
                    break;
            }
            result.then(function (response) {
                if (Err.errorRequest(response.data)) {
                    Friends.setFriendList();
                    Err.notify(response.data.message);
                }
            });
        };

        $scope.viewStory = function (story) {
            $state.go('viewstory', { story: story });
        };

        $scope.viewDetail = function (friend) {
            $state.go('tabs.friendprofile', { friend: friend });
        };

        $scope.showUploadOption = function () {
            $ionicActionSheet.show({
                buttons: [
                  { text: 'Take Photo' },
                  { text: 'Choose From Album' }
                ],
                destructiveText: 'Remove Profile Picture',
                titleText: 'Update Profile Picture',
                cancelText: 'Cancel',
                buttonClicked: function (index) {
                    if (index === 0) {
                        takePhoto();
                        return true;
                    }
                    else if (index == 1) {
                        importPhoto();
                        return true;
                    }
                },
                destructiveButtonClicked: function () {
                    removeProfilePicture();
                    return true;
                }
            });
        };

        $ionicModal.fromTemplateUrl('templates/popup-templates/modal-update-user.html', {
            scope: $scope,
            animation: 'slide-in-up',
            backdropClickToClose: false
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.showModal = function () {
            $ionicModal.fromTemplateUrl('templates/popup-templates/modal-update-user.html', {
                scope: $scope,
                animation: 'slide-in-up',
                backdropClickToClose: false
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };

        //Perform checking to enable/disable change password operation
        $scope.isNormalLogin = function () {
            return (angular.fromJson(localStorage.facebook) === null && angular.fromJson(localStorage.google) === null);
        };

        $scope.logout = function () {
            UserService.logout();
            $state.go('login');
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
                disableAnimate: false,
                disableBack: true
            });
            $scope.modal.hide();
            $scope.modal.remove();
            $window.location.reload(); //Re-render whole page on logout to restart controller + services
        };

        $scope.displayNameUpdatePopup = function () {
            var updateDisplayName = $ionicPopup.prompt({
                template: 'Enter new display name below',
                title: 'Change your display name',
                inputPlaceholder: 'Enter name here',
            });

            updateDisplayName.then(function (res) {
                if (res.length > 0 && res.length < 20) {
                    UserService.updateName(res)
                    .then(function (response) {
                        UserService.setSession();
                    });
                } else {
                    Err.notify('The name is empty or too long (max 20 characters)!');
                }
            });
        };

        $scope.passwordUpdatePopup = function () {
            var updatePassword = $ionicPopup.show({
                templateUrl: 'templates/popup-templates/change-password-popup.html',
                title: 'Change your Password',
                scope: $scope,
                buttons: [
                  { text: 'Cancel' },
                  {
                      text: '<b>Ok</b>',
                      type: 'button-positive',
                      onTap: function (e) {
                          if (!($scope.data.oldPassword && $scope.data.newPassword && $scope.data.cNewPassword)) {
                              Err.notify("Please fill in all the field !");
                              e.preventDefault();
                          } else {
                              return {
                                  oldPass: $scope.data.oldPassword,
                                  newPass: $scope.data.newPassword,
                                  cNewPass: $scope.data.cNewPassword
                              };
                          }
                      }
                  }
                ]
            });

            updatePassword.then(function (res) {
                if (res.newPass == res.cNewPass) {
                    if (res.newPass.length < 6) {
                        UserService.changePassword(res.oldPass, res.newPass).then(function (response) {
                            if (Err.errorRequest(response.data)) {
                                Err.notify('Password Updated');
                            }
                        });
                    } else {
                        Err.notify('Password too short, minumum six character !');
                    }
                }
                else {
                    Err.notify('Password not matched');
                }
            });
        };

        $scope.statusUpdatePopup = function () {
            var updateStatus = $ionicPopup.prompt({
                template: 'Enter your status below',
                title: 'Change your status',
                inputPlaceholder: 'Write something about yourself !'
            });

            updateStatus.then(function (res) {
                if (res.length > 0 && res.length < 150) {
                    UserService.updateStatus(res)
                    .then(function (response) {
                        if (Err.errorRequest(response.data)) {
                            UserService.setSession();
                        }
                    });
                } else {
                    Err.notify("The status field is empty or too long (max 150 characters)!");
                }
            });
        };

        /*Function for different action in action sheet*/
        function takePhoto() {
            $cordovaCamera.getPicture({
                quality: 100,
                targetHeight: 200,
                targetWidth: 250,
                correctOrientation: true,
            })
            .then(function (data) {
                uploadPhoto(data);
            }, function (error) {
                Err.unknownError();
            });
        }

        function importPhoto() {
            $cordovaImagePicker.getPictures({
                maximumImagesCount: 1,
                width: 200,
                height: 250,
                quality: 100
            }).then(function (results) {
                uploadPhoto(results[0]);
            }, function (error) {
                Err.unknownError();
            });
        }

        function uploadPhoto(pictureUrl) {
            UserService.changeProfilePicture(pictureUrl).then(function (result) {
                UserService.setSession();
            }, function (error) {
                Err.notify("Unable to update profile picture currently..");
            }, function (progress) {
                if ((progress.loaded / progress.total) * 100 !== 100) {
                    Err.wait();
                } else {
                    Err.stop();
                }
            });
        }

        function removeProfilePicture() {
            UserService.removeProfilePicture().then(function (response) {
                if (Err.errorRequest(response.data)) {
                    UserService.setSession();
                }
            });
        }
    }]);