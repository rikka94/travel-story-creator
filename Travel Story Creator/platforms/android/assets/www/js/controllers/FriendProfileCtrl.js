angular.module('travelstory.controllers')

.controller('FriendProfileCtrl', ['$scope', '$stateParams', 'Friends', 'ErrorHandler', 'DEFAULT_PP',
    function ($scope, $stateParams, Friends, Err, DEFAULT_PP) {

        $scope.friendInfo = $stateParams.friend;
        $scope.defaultProfilePicture = DEFAULT_PP;

        $scope.friendOperation = function (operation) {
            var friend_id = $scope.friendInfo.id;
            var result;
            switch (operation) {
                case 'add':
                    result = Friends.addFriend(friend_id);
                    break;
                case 'update':
                    result = Friends.acceptFriend(friend_id);
                    break;
                case 'delete':
                    result = Friends.removeFriend(friend_id);
                    break;
            }

            result.then(function (response) {
                if (Err.errorRequest(response.data)) {
                    switch (response.data.message) {
                        case 'Friend added':
                            $scope.friendInfo.status = "sent";
                            break;
                        case 'Friend accepted':
                            $scope.friendInfo.status = "friend";
                            break;
                        case 'Friend removed':
                            $scope.friendInfo.status = "";
                            break;
                    }
                    Err.notify(response.data.message);
                }
            });
        };
    }]);