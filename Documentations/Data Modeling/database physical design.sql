-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
-- Host: 128.199.96.57
-- Generation Time: Aug 25, 2015 at 02:26 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS travel_story_creator;
-- Database: `travel_story_creator`

-- --------------------------------------------------------
USE zz;
-- Table structure for table `friendship`

CREATE TABLE IF NOT EXISTS `friendship` (
  `user_id` int(11) unsigned NOT NULL,
  `friend_id` int(11) unsigned NOT NULL,
  `status` varchar(8) NOT NULL,
  PRIMARY KEY (`user_id`,`friend_id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
-- Table structure for table `photos`
CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `story_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `story_id` (`story_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
-- Table structure for table `story`
CREATE TABLE IF NOT EXISTS `story` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `cover_photo` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `story_id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
-- Table structure for table `users`
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT 'Hello, I am using Travel Story Creator !',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `user_id` (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- Constraints for table `friendship`
ALTER TABLE `friendship`
  ADD CONSTRAINT `FK_user1_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_user2_user_id` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`);

-- Constraints for table `photos`
ALTER TABLE `photos`
  ADD CONSTRAINT `FK_story_photo_id` FOREIGN KEY (`story_id`) REFERENCES `story` (`id`);

-- Constraints for table `story`
ALTER TABLE `story`
  ADD CONSTRAINT `FK_user_story_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
